var User = require('../model/user-model')

exports.adminAuth = (req, res, next) => {
    let id = req.header('x-auth-id')
    let user = User.findOne({ _id: id }, (err, user) => {
        if (user.role == true) {
            next()
        } else {
            return res.status(403).send()
        }
    })
}