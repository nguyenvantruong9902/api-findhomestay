'use strict';

const nodemailer = require('nodemailer');
const emailExistence = require('email-existence');
const notification = require('../../check/notification');
const encodeData = require('../../utils/encode-data');
const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class SendMail {
    async sendMail(email, data, res) {
        if (regEx.test(email)) {
            let checkEmail = await this.checkExistsMail(email);
            if (checkEmail) {
                let transporter = nodemailer.createTransport({
                    host: "smtp.gmail.com",
                    port: 587,
                    secure: false,
                    auth: {
                        user: 'findhomestay02@gmail.com',
                        pass: 'monduan2'
                    }
                });

                let mailOptions = {
                    from: '<findhomestay02@gmail.com> Tìm Trọ Đà Nẵng',
                    to: email,
                    subject: "Xác minh email của bạn cho việc Đăng ký tài khoản Tìm Trọ Đà Nẵng",
                    html: `<h1>Hello</h1>${data}`,
                    
                };
                await transporter.sendMail(mailOptions, (err, info) => {
                    if (err) {
                        console.log(err);
                        res.send({
                            error: 'Lỗi gửi mail'
                        })
                    } else {
                        res.send({
                            user: {
                                success: encodeData.encryptData(data)
                            }
                        })
                    }
                });
            } else {
                res.send({
                    error: notification.error_email_not_exist
                })
            }
        } else {
            res.send({
                error: notification.error_email_invalid
            })
        }
    }

    checkExistsMail(email) {
        return new Promise((resolve, reject) => {
            emailExistence.check(email, function (error, response) {
                resolve(response)
            });
        })
    }
}

module.exports = new SendMail();