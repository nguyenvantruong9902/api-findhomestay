'use strict';
const fs = require('fs');
const path = require('path');
const drive = require('../drive/config-accout');

const dir = './files';

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

class DownloadFileOnDrive {
    downloadFile(request, response) {
        let data = request.body;
        let dest = fs.createWriteStream(`${dir}/` + data.fileName);

        drive.files.get({
            fileId: data.fileId,
            alt: 'media'
        }, { responseType: 'stream' }, (err, res) => {
            res.data.on('end', function () {
                let filePath = path.join(__dirname, '../../../files/' + data.fileName);
                response.sendFile(filePath);
                dest.on('close', () => {
                    setTimeout(() => {
                        dest.destroy();
                        try {
                            fs.unlinkSync(filePath);
                        } catch (error) {

                        }
                    }, 3000);
                })
            }).on('error', function (err) {
                console.log('Error during download', err);
            }).pipe(dest)
        })

    }
}

module.exports = new DownloadFileOnDrive();