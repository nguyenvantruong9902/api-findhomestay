'use strict';
const stream = require('stream');
const drive = require('../drive/config-accout');
const key = require('../keys');

const folderId = key.folderId;


class UploadFileOnDrive {
    async uploadFile(req, res) {
        const file = await drive.files.create(
            {
                requestBody: {
                    name: req.file.originalname,
                    mimeType: '*/*',
                    parents: [folderId]
                },
                media: {
                    mimeType: '*/*',
                    body: new stream.PassThrough().end(req.file.buffer)
                }
            },
            {
                onUploadProgress: (e) => {}
            }
        );
        res.send({
            file: {
                fileId: file.data.id,
                fileName: file.data.name
            }
        });
    }

    // delete = async function deleteImage(image) {
    //     await drive.files.delete(
    //         {
    //             fileId: image,
    //         }
    //     );
    // }
}

module.exports = new UploadFileOnDrive();