const key = require('../keys');
const { google } = require('googleapis');

const googleClientId = key.clientID;
const googleClientSecret = key.clientSecret;
const googleRefreshToken = key.refreshToken;

const oauth2Client = new google.auth.OAuth2(googleClientId, googleClientSecret, 'http://localhost:3000');
oauth2Client.setCredentials({ refresh_token: googleRefreshToken });

const drive = google.drive({
    version: 'v3',
    auth: oauth2Client
});

module.exports = drive;