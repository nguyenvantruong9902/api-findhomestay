'use strict';
const userCtrl = require('../controller/user-ctrl');
const findUser = require('../check/find-user');
const messageCtrl = require('../controller/message-ctrl');
const notification = require('../check/notification');
const encodeData = require('../utils/encode-data');

class Socket {

    constructor(socket) {
        this.io = socket;
    }

    socketEvents() {
        this.io.on('connection', (socket) => {
            //Chat list
            socket.on('chat-list', async (data) => {
                try {
                    let userId = encodeData.decryptData(data.id);
                    var chatListResponse = await Promise.resolve(userCtrl.userGetFollowings(userId));
                    this.io.to(socket.id).emit(`chat-list-response`, {
                        error: false,
                        singleUser: false,
                        chatList: encodeData.encryptData(JSON.stringify(chatListResponse))
                    });
                    var userInfoResponse = await Promise.resolve(findUser.findIdUser(userId));
                    socket.broadcast.emit(`chat-list-response`, {
                        error: false,
                        singleUser: true,
                        chatList: encodeData.encryptData(JSON.stringify(userInfoResponse)),
                        userId: encodeData.encryptData(userId)
                    });
                } catch (error) {
                    this.io.to(socket.id).emit(`chat-list-response`, {
                        chatList: []
                    });
                    console.log('Lỗi getChatList');
                }
            });

            //Send message
            socket.on('add-message', async (data) => {
                data = JSON.parse(encodeData.decryptData(data));
                if (data.message.text == '' && data.message.files.length == 0) {
                    this.io.to(socket.id).emit(`add-message-response`, {
                        error: true,
                        message: notification.empty_message
                    });
                } else if (data.fromUserId == '') {
                    this.io.to(socket.id).emit(`add-message-response`, {
                        error: true,
                        message: notification.error_connect_server
                    });
                } else if (data.toUserId == '') {
                    this.io.to(socket.id).emit(`add-message-response`, {
                        error: true,
                        message: notification.error_select_user
                    });
                } else {
                    try {
                        var toSocketId = await Promise.resolve(findUser.findIdUser(data.toUserId));
                        messageCtrl.insertMessage(data);
                        this.io.to(toSocketId.socketId).emit(`add-message-response`,
                            encodeData.encryptData(JSON.stringify(data)));
                    } catch (error) {
                        this.io.to(socket.id).emit(`add-message-response`, {
                            error: true,
                            message: notification.error_connect_server
                        });
                    }
                }
            });

            socket.on('typing', async (data) => {
                data.toUserId = encodeData.decryptData(data.toUserId);
                try {
                    var toSocketId = await Promise.resolve(findUser.findIdUser(data.toUserId));
                    if (toSocketId) {
                        this.io.to(toSocketId.socketId).emit(`typing-response`, data);
                    }
                } catch (error) {
                    console.log('Lỗi typing socket')
                }
            })

            socket.on('last-message', async (data) => {
                data.fromUserId = encodeData.decryptData(data.fromUserId);
                data.toUserId = encodeData.decryptData(data.toUserId);
                try {
                    var [toSocketId, lastMessage] = await Promise.all([findUser.findIdUser(data.fromUserId), messageCtrl.getLastMessage(data)]);
                    if (toSocketId) {
                        this.io.to(toSocketId.socketId).emit(`last-message-response`, lastMessage);
                    }
                } catch (error) {
                    console.log('Lỗi typing socket')
                }
            })

            //Add user offline not following
            socket.on('add-user-offline', async (data) => {
                try {
                    var chatListResponse = await Promise.resolve(userCtrl.userGetFollowings(data.toUserId));
                    var notFollow = chatListResponse.findIndex(element => element._id == data.fromUserId);
                    if (notFollow < 0) {
                        var [toSocketId, fromSocketId] = await Promise.all([
                            findUser.findIdUser(data.toUserId), findUser.findIdUser(data.fromUserId)]);
                        this.io.to(toSocketId.socketId).emit(`add-user-offline-response`,
                            { isFollow: false, user: fromSocketId });
                    }
                } catch (error) {
                    console.log('Lỗi add-user-offline');
                }
            })

            //Log out
            socket.on('logout', async (data) => {
                try {
                    var userId = encodeData.decryptData(data.userId);
                    await userCtrl.userLogout(userId);
                    this.io.to(socket.id).emit(`logout-response`, {
                        error: false,
                        message: 'Logout',
                        userId: userId
                    });
                    socket.broadcast.emit(`chat-list-response`, {
                        error: false,
                        userDisconnected: true,
                        userid: userId
                    });
                } catch (error) {
                    console.log('Lỗi logout');
                }
            });

            //Disconnect user
            socket.on('disconnect', async () => {
                let userId = socket.request._query['id'];
                await userCtrl.userLogout(encodeData.decryptData(userId));
                socket.broadcast.emit(`chat-list-response`, {
                    error: false,
                    userDisconnected: true,
                    userId: encodeData.encryptData(userId),
                    timeOff: new Date().getTime()
                });
            });
        })
    }

    socketConfig() {
        this.io.use(async (socket, next) => {
            try {
                let userId = encodeData.decryptData(socket.request._query['id']);
                if (userId) {
                    userCtrl.userAddSocketId({
                        userId: userId,
                        socketId: socket.id
                    });
                }
                next();
            } catch (error) {
                console.error('Lỗi socketConfig');
            }
        });
        this.socketEvents();
    }
}

module.exports = Socket;