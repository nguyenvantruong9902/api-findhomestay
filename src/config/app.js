'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const key = require('./keys');
const socketio = require('socket.io');
const http = require('http');
const passport = require('passport');

require('./google/passport');
require('../config/mongodb/connect');
const deleteUsers = require('../check/delete-user');
const authRoutes = require('../routes/auth-routes');
const userRoutes = require('../routes/user-routes');
const messageRoutes = require('../routes/message-routes');
const uploadFileRoutes = require('../routes/file-routes');
const postRoutes = require('../routes/post-routes');
const socketConfig = require('../config/socket');

class App {
    constructor() {
        this.app = express();
        this.http = http.Server(this.app);
        this.socket = socketio(this.http);
    }
    delete() {
        deleteUsers();
    }
    appConfig() {
        this.app.use(cors());
        this.app.use(bodyParser.json());
        this.app.use(passport.initialize());
        this.http.listen(key.port);
    }
    includeRoutes() {
        new socketConfig(this.socket).socketConfig();
        this.app.use('/auth', authRoutes);
        this.app.use('/user', userRoutes);
        this.app.use('/message', messageRoutes);
        this.app.use('/file', uploadFileRoutes);
        this.app.use('/post', postRoutes);
    }
    execute() {
        this.delete();
        this.appConfig();
        this.includeRoutes();
    }
}

module.exports = new App();