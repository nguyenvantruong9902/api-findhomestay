'use strict';

const Nexmo = require('nexmo');
const key = require('../keys');
const notification = require('../../check/notification');
const nexmo = new Nexmo({
    apiKey: key.apiKeyNexmo,
    apiSecret: key.apiSecretNexmo
});

class SendPhone {
    //Send the code
    sendPhone(phoneNumber, res) {
        new Promise((resolve, reject) => {
            if (phoneNumber) {
                nexmo.verify.request({
                    number: phoneNumber,
                    brand: 'Nexmo',
                    code_length: '6'
                }, (err, result) => {
                    if (err) {
                        res.send({
                            error: notification.error_connect_server
                        });
                    }
                    else {
                        res.send({
                            user: {
                                success: notification.success_send_phone
                            }
                        });
                        resolve(result.request_id);
                    }
                });
            } else {
                res.send({
                    error: notification.empty_phone_number
                });
            }
        })
    }

    //Verify the code
    verifyCode(request_id, code, res) {
        new Promise((resolve, reject) => {
            if (request_id) {
                if (code) {
                    nexmo.verify.check({
                        request_id: request_id,
                        code: code
                    }, (err, result) => {
                        if (err) {
                            res.send({
                                error: notification.error_connect_server
                            })
                        }
                        else {
                            res.send({
                                success: notification.success_verify_phone
                            });
                            resolve(result);
                        }
                    });
                }
            }
        })
    }
}

module.exports = new SendPhone();