'use strict';
const passport = require('passport');
const notification = require('../../check/notification');
const encodeData = require('../../utils/encode-data');

class AuthCtroller {
    authenticate(req, res, next) {
        passport.authenticate('google', {
            scope: ['profile', 'email']
        })(req, res, next)
    }

    authenticateCallBack(req, res, next) {
        passport.authenticate('google', (err, user) => {
            if (user) {
                let data = {
                    id: user._id.toString(),
                    name: user.name,
                    role: user.isRole
                }
                res.redirect('http://localhost:4200/google/info/' + encodeData.encryptData(JSON.stringify(data)));
            }
            if (err) {
                console.log('Lỗi authenticateCallBack');
            }
        })(req, res, next)
    }
}

module.exports = new AuthCtroller();
