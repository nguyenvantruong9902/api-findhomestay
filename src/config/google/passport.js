'use strict';
const passport = require('passport');
const uid = require('uid');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const User = require('../../model/user-model');
const passwordHash = require('../../utils/password-hash');
const key = require('../keys');
const findUser = require('../../check/find-user');

passport.use(
    new GoogleStrategy({
        clientID: key.clientID,
        clientSecret: key.clientSecret,
        callbackURL: '/auth/google/callback'
    }, async (accessToken, refreshToken, profile, done) => {
        if (profile.id) {
            let email = profile.emails[0].value;
            let image = profile.photos[0].value;
            let name = profile.displayName;
            var findEmail = await findUser.findEmailUser(email);
            if (findEmail) {
                if (findEmail.image != undefined && findEmail.image != '' && findEmail.image != null) {
                    User.updateOne({ _id: findEmail._id }, { image: image }).then().catch(err => console.log('Lỗi updateUserGoogle'));
                }
                done(null, findEmail);
            } else {
                let data = {
                    email: email,
                    image: image,
                    name: name,
                    password: passwordHash.createHash(uid(10)),
                    isVerify: true
                }
                var user = new User(data);
                user.save().then().catch(err => console.log('Lỗi lưu user passport'));
                done(null, user);
            }
        }
    })
);

module.exports = passport;