const mongoose = require('mongoose');
const database = require('./database');

mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;
mongoose.connect(database.url, { useNewUrlParser: true });

module.exports = mongoose;