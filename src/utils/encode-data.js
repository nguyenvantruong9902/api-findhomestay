'use strict';

const CryptoJS = require('crypto-js');
const key = require('../config/keys');

class EncodeData {
    encryptData(data) {
        try {
            return CryptoJS.AES.encrypt(data, key.keySecret.trim()).toString();
        } catch (error) {
            console.log('Lỗi encryptData');
        }
    }

    decryptData(encryptData) {
        try {
            return CryptoJS.AES.decrypt(encryptData, key.keySecret.trim()).toString(CryptoJS.enc.Utf8);
        } catch (error) {
            console.log('Lỗi decryptData');
        }
    }

    // encryptObject(object) {
    //     return CryptoJS.AES.encrypt(JSON.stringify(object), key.keySecret.trim());
    // }

    // getBytesWords(encryptObject) {
    //     return CryptoJS.AES.decrypt(encryptObject, key.keySecret.trim());
    // }
}

module.exports = new EncodeData();