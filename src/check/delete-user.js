'use strict';

const findUser = require('./find-user');

module.exports = async () => {
    var allUser = await findUser.findAllUser();
    var now = new Date();
    if (allUser) {
        allUser.forEach(user => {
            if (!user.isVerify && now - user.dateRegister.getTime() >= 86400000) {
                user.deleteOne({ _id: user._id }).then().catch(console.error);
            }
        });
    }
}
