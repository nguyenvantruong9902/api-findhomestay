const User = require('../model/user-model');

'use strict';
class FindUser {

    //Get user
    findIdUser(value) {
        return new Promise((resolve, reject) => {
            User.findOne({ _id: value }).then(users => resolve(users)).catch(err => console.log('Lỗi findIdUser'));
        });
    }

    //Get phone number
    findPhoneUser(value) {
        return new Promise((resolve, reject) => {
            User.findOne({ sdt: value }).then(users => resolve(users)).catch(err => console.log('Lỗi findPhoneUser'));
        });
    }

    findEmailUser(value) {
        return new Promise((resolve, reject) => {
            User.findOne({ email: value }).then(users => resolve(users)).catch(err => console.log('Lỗi findEmailUser'));
        });
    }

    findAllUser() {
        return new Promise((resolve, reject) => {
            User.find().then(users => resolve(users)).catch(err => console.log('Lỗi findAllUser'));
        });
    }
}

module.exports = new FindUser();