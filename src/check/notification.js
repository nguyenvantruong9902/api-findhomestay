
module.exports = {
    //Success
    success_register: 'Đăng kí thành công',
    success_send_email: 'Vui lòng kiểm tra email',
    success_login: 'Đăng nhập thành công',
    success_send_phone: 'Vui lòng kiểm tra điện thoại',
    success_verify_phone: 'Xác thực thành công',
    success_deleted: 'Đã xóa',

    //Error
    error_email_exist: 'Email đã được sử dụng!',
    error_email_not_exist: 'Không tìm thấy email!',
    error_email_invalid: 'Email không đúng định dạng!',
    error_register_fail: 'Đăng kí thất bại!',
    error_send_mail_fail: 'Gửi mail thất bại!',
    error_connect_server: 'Lỗi kết nối với máy chủ!',
    error_password: 'Mật khẩu không chính xác!',
    error_select_user: 'Chọn người để chat!',
    error_verify: 'Mã xác nhận không chính xác!',
    error_phone_exist: 'Số điện thoại đã được sử dụng',

    //Empty
    empty_message: 'Tin nhắn rỗng!',
    empty_phone_number: 'Số điện thoại rỗng',

    //Length
    error_length_password: 'Mật khẩu ít nhất 8 kí tự',
}