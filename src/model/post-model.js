const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let Post = new Schema({
    IdUser: {
        type: String
    },
    files: [{
        _id: false,
        fileId: String,
        fileName: String,
        fileType: String
    }],
    Title: {
        type: String
    },
    Contact: {
        type: String
    },
    Description: {
        type: String
    },
    Price: {
        type: String
    },
    Address: {
        type: String,
    },
    Status: {
        type: Boolean,
        default: true
    },
    Approval: {
        type: Boolean,
        default: false
    },
    Date: {
        type: Date,
        default: new Date()
    }
});
module.exports = mongoose.model('post', Post);