const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Message = new Schema({
    fromUserId: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    message: {
        text: String,
        files: [
            {
                _id: false,
                fileId: String,
                fileName: String,
                fileType: String
            }
        ]
    },
    toUserId: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    date: {
        type: Date,
        default: new Date()
    },
    time: {
        type: String,
        default: new Date().getTime()
    },
    deleted: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    isView: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model('message', Message);