const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let User = new Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    BirthDay: {
        type: Date,
        default: new Date()
    },
    password: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
    },
    isVerify: {
        type: Boolean,
        default: false
    },
    dateRegister: {
        type: Date,
        default: new Date()
    },
    isRole: {
        type: Boolean,
        default: false
    },
    followers: [{
        type: Schema.Types.ObjectId,
        ref: 'users'
    }],
    following: [{
        _id: false,
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'users',
        },
        dateFollow: {
            type: Date,
            default: new Date()
        }
    }],
    image: {
        type: String
    },
    isOnline: {
        type: Boolean,
        default: true
    },
    socketId: {
        type: String
    },
    isBlock: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('users', User);