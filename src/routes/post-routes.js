const express = require('express');
const route = express.Router();
const postctrl = require('../controller/post-ctrl')

route.post('/addpost', postctrl.SavePost);
route.post('/listpost', postctrl.ListPost);
route.post('/getpostbyid', postctrl.FindbyID);
route.post('/getpostpersonal', postctrl.getPostPersonal)
module.exports = route;