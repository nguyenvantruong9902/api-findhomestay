const express = require('express');
const route = express.Router();
const messageCtrl = require('../controller/message-ctrl');

route.post('/getMessages', messageCtrl.getMessages);
route.post('/getUsers', messageCtrl.getUsersMessage);
route.post('/updateIsView', messageCtrl.updateIsViewMessage);
route.post('/delete',messageCtrl.deleteMessages);

module.exports = route;