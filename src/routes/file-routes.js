const express = require('express');
const route = express.Router();
const upload = require('../config/drive/upload-file');
const download = require('../config/drive/download-file');
const storage = require('../config/drive/config-storage');

//Upload file to drive
route.post('/upload', storage.single('files'), upload.uploadFile);
//Download file to drive
route.post('/download', download.downloadFile);
route.post('/uploadimagePost', storage.single('image'), upload.uploadFile);
route.post('/uploadimageUser', storage.single('image'), upload.uploadFile);
module.exports = route;