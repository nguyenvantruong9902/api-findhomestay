'use strict';

const express = require('express');
const route = express.Router();
const authCtrl = require('../config/google/auth-ctrl');

route.get('/google', authCtrl.authenticate);
route.get('/google/callback', authCtrl.authenticateCallBack);

module.exports = route;