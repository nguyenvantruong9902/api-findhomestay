const express = require('express');
const route = express.Router();
const userCtrl = require('../controller/user-ctrl');

//Send mail
route.post('/sendmail', userCtrl.userSendMailVerify);
//Send phone number
route.post('/sendphone', userCtrl.userSendPhoneNumber);
//Verify phone number
route.post('/verifyphone', userCtrl.userVerifyPhoneNumber);
//Register
route.post('/register', userCtrl.userRegister);
//Verification
route.post('/verify', userCtrl.userVerification);
//Login
route.post('/login', userCtrl.userLogin);
//User info
route.post('/info', userCtrl.userGetInfo);
//Get all user following
route.post('/following', userCtrl.userGetAllFollowings);
//update information
route.post('/updateuser', userCtrl.userUpdateInformation);

module.exports = route;