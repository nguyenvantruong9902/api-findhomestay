'use strict';

const uid = require('uid');
const User = require('../model/user-model');
const notification = require('../check/notification');
const findUser = require('../check/find-user');
const passwordHash = require('../utils/password-hash');
const sendMail = require('../config/mail/send-mail');
const sendPhone = require('../config/phone/send-phone');
const encodeData = require('../utils/encode-data');

class UserController {
    constructor() {}

    //Add socket
    userAddSocketId({ userId, socketId }) {
        User.updateOne({ _id: userId }, { socketId: socketId, isOnline: true }).then().catch(err => console.log('Err userAddSocketId'));
    }

    //Send mail
    async userSendMailVerify(req, res) {
            let data = req.body;
            var findEmail = await Promise.resolve(findUser.findEmailUser(data.email));
            if (findEmail) {
                if (findEmail.isVerify) {
                    res.send({
                        error: notification.error_email_exist
                    })
                } else {
                    var now = new Date();
                    if (data.sm != '' && data.sm != null && data.sm != undefined && encodeData.decryptData(data.sm) == 'sendmail' && now.getTime() - findEmail.dateRegister.getTime() < 180000) {
                        sendMail.sendMail(data.email, encodeData.decryptData(findEmail.password), res);
                    } else {
                        sendMail.sendMail(data.email, uid(10), res);
                    }
                }
            } else {
                sendMail.sendMail(data.email, uid(10), res);
            }
        }
        //Register
    async userRegister(req, res) {
        let data = req.body;
        data.password = encodeData.encryptData(data.password);
        var findEmail = await findUser.findEmailUser(data.email);
        if (!findEmail) {
            let user = new User(data);
            user.save().then(user => {
                res.send({
                    user: {
                        success: notification.success_send_email
                    }
                })
            }, err => {
                res.send({
                    error: notification.error_send_mail_fail
                })
            })
        } else {
            if (findEmail.isVerify) {
                res.send({
                    error: notification.error_email_exist
                })
            } else {
                var now = new Date();
                if (encodeData.decryptData(data.password) != encodeData.decryptData(findEmail.password)) {
                    User.updateOne({ email: findEmail.email }, { dateRegister: now, password: data.password }).then().catch(err => console.log('Err updateRegister'));
                }
                res.send({
                    user: {
                        success: notification.success_send_email
                    }
                })
            }
        }
    }

    async userVerification(req, res) {
        let data = req.body;
        var findEmail = await findUser.findEmailUser(data.email);
        var verification = encodeData.decryptData(findEmail.password);
        if (findEmail) {
            if (verification === data.verification) {
                if (data.password.length >= 8) {
                    data.password = passwordHash.createHash(data.password);
                    User.updateOne({ email: findEmail.email }, { name: data.name, password: data.password, isVerify: true }).then(user => {
                        res.send({
                            user: {
                                id: encodeData.encryptData(findEmail._id.toString()),
                                success: notification.success_register
                            }
                        })
                    }, err => {
                        res.send({
                            error: notification.error_register_fail
                        })
                    });
                } else {
                    res.send({
                        error: notification.error_length_password
                    })
                }
            } else {
                res.send({
                    error: notification.error_verify
                })
            }
        }
    }

    //Login
    async userLogin(req, res) {
        let data = req.body;
        data.password = encodeData.decryptData(data.password);
        var findEmail = await findUser.findEmailUser(data.email);
        if (findEmail && findEmail.isVerify) {
            if (passwordHash.compareHash(data.password, findEmail.password)) {
                let user = {
                    id: findEmail._id.toString(),
                    name: findEmail.name,
                    role: findEmail.isRole
                }
                res.send({
                    user: {
                        info: encodeData.encryptData(JSON.stringify(user)),
                        success: notification.success_login
                    }
                });
            } else {
                res.send({
                    error: notification.error_password
                });
            }
        } else {
            res.send({
                error: notification.error_email_not_exist
            });
        }
    }

    //Log out
    userLogout(userId) {
        User.updateOne({ _id: userId }, { isOnline: false }).then().catch(console.error);
    }

    //Send phone number
    async userSendPhoneNumber(req, res) {
        let data = req.body;
        var findPhone = await findUser.findPhoneUser(data.phone);
        if (findPhone) {
            res.send({
                error: notification.error_phone_exist
            });
        } else {
            var requestId = await sendPhone.sendPhone(data.phoneNumber, res);
            if (requestId) {
                User.updateOne({ _id: data.id }, { phone: requestId });
            }
        }
    }

    //Verify phone number
    async userVerifyPhoneNumber(req, res) {
        let data = req.body;
        data.id = encodeData.decryptData(data.id);
        var findId = await findUser.findIdUser(data.id);
        if (findId) {
            var result = await sendPhone.verifyCode(findId.phone, data.code, res);
            if (result) {
                User.updateOne({ _id: data.id }, { phone: data.phone }).then().catch(console.error);
            }
        }
    }

    //Get following of user by socketId
    userGetFollowings(userId) {
        return new Promise(async(resolve, reject) => {
            User.findOne({ _id: userId }, async(err, user) => {
                if (user) {
                    let userFollowings = [];
                    let length = user.following.length;
                    let arrFollowing = user.following;
                    for (var i = 0; i < length; i++) {
                        var follow = await Promise.resolve(findUser.findIdUser(arrFollowing[i].userId));
                        if (follow && follow.isOnline) {
                            userFollowings.push(follow);
                        }
                    }
                    resolve(userFollowings);
                }
                if (err) {
                    console.log('Err userGetFollowings');
                }
            })
        })
    }

    userGetAllFollowings(req, res) {
        User.findOne({ _id: encodeData.decryptData(req.body.userId) }, async(err, user) => {
            if (user) {
                let userFollowings = [];
                let length = user.following.length;
                let arrFollowing = user.following;
                for (var i = 0; i < length; i++) {
                    var follow = await Promise.resolve(findUser.findIdUser(arrFollowing[i].userId));
                    if (follow) {
                        userFollowings.push(follow);
                    }
                }
                res.send({
                    data: encodeData.encryptData(JSON.stringify(userFollowings))
                })
            }
            if (err) {
                console.log('Err userGetAllFollowings');
            }
        })
    }

    //Get infomation
    async userGetInfo(req, res) {
        let id = encodeData.decryptData(req.body.id);
        if (id) {
            var findId = await findUser.findIdUser(id);
            if (findId) {
                res.send({
                    data: encodeData.encryptData(JSON.stringify(findId))
                });
            } else {
                console.log('Err userGetInfo');
            }
        } else {
            res.send({
                data: null
            })
        }
    }

    //Add follower
    userAddFollower() {
        User.findOne({ _id: '5d628c7ff7e89e25fcc233ce' }, async(err, user) => {
            var follower = await Promise.resolve(findUser.findIdUser('5d6284e35be5b629d805f579'));
            user.followers.push(follower);
            user.save().then().catch(console.error);
        })
    }

    //Delete follower
    userDeleteFollower() {
        User.findOne({ _id: '5d650e9c689424265cdcf651' }, async(err, user) => {
            user.followers.pull('5d650e61689424265cdcf650');
            user.save().then().catch(console.error);
        })
    }

    //Add following
    async userAddFollowing() {
        var following = await Promise.resolve(findUser.findIdUser('5da14aaa5a490115fc8ab978'));
        if (following) {
            User.updateOne({ _id: '5da149225a490115fc8ab977' }, { $push: { following: { userId: following } } }, { safe: true, multi: true }, function(err, user) {
                if (err) {
                    console.log('Err userAddFollowing');
                }
            });
        }
    }

    //Delete following
    userDeleteFollowing() {
        User.updateOne({ _id: '5d9aad7ae77cca0750958f3c' }, { $pull: { following: { userId: '5d9ab076e77cca0750958f3d' } } }, { safe: true, multi: true }, function(err, user) {
            if (err) {
                console.log('Err userDeleteFollowing');
            }
        });
    }
    userUpdateInformation(req, res) {
        let data = req.body;
        User.updateOne({ _id: data._id }, { name: data.name, email: data.email, phone: data.phone, BirthDay: data.BirthDay, image: data.image }).then(
            user => {
                res.send({
                    success: 'Update Thành Công!'
                })
            }, err => {
                res.send({
                        error: "Update Thất Bại"
                    }),
                    console.log(err)
            })
    }
}

module.exports = new UserController();