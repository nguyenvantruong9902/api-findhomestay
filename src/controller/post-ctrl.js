'use strict';
const Post = require('../model/post-model');
const encodeData = require('../utils/encode-data');
class PostController {
    SavePost(req, res) {
        let data = req.body
        let post = new Post(data);
        post.save().then(
            post => {
                res.send({
                    success: 'Thêm Bài Viết Thành Công'
                })
            }, err => {
                res.send({
                    error: "Lỗi Khi Thêm Bài Viết"
                })
            }
        )

    }
    ListPost(req, res) {
        let Approval = req.body.Approval
        Post.find({ Approval: Approval }).then(Post =>
            res.send({
                data: encodeData.encryptData(JSON.stringify(Post))
            })).catch(err =>
            console.log(err));
    }
    FindbyID(req, res) {
        let ID = req.body._id
        Post.findOne({ _id: ID }).then(Post =>
            res.send({
                data: encodeData.encryptData(JSON.stringify(Post))
            })).catch(err =>
            console.log(err));
    }
    getPostPersonal(req, res) {
        let IdUser = req.body.IdUser
        let Approval = req.body.Approval
        Post.find({ IdUser: IdUser, Approval: Approval }).then(Post =>
            res.send({
                data: encodeData.encryptData(JSON.stringify(Post))
            })).catch(err =>
            console.log(err));
    }
}
module.exports = new PostController();