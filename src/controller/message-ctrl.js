'use strict';

const Message = require('../model/message-model');
const findUser = require('../check/find-user');
const encodeData = require('../utils/encode-data');
const notification = require('../check/notification');

class MessageController {
    // Insert message
    insertMessage(message) {
        if (message) {
            new Message(message).save().then().catch(err => 'Lỗi insertMessage');
        } else {
            console.log('Lỗi insertMessage')
        }
    }

    // Get messages
    getMessages(req, res) {
        let data = req.body;
        let fromUserId = encodeData.decryptData(data.fromUserId);
        let toUserId = encodeData.decryptData(data.toUserId);
        Message.find({
            $or: [
                {
                    $and: [
                        { fromUserId: fromUserId, toUserId: toUserId }
                    ]
                },
                {
                    $and: [
                        { fromUserId: toUserId, toUserId: fromUserId }
                    ]
                }
            ]
        }, (err, messages) => {
            if (messages) {
                let messagesNotDelete = [];
                for (let i = 0; i < messages.length; i++) {
                    if (messages[i].deleted == undefined || messages[i].deleted != fromUserId) {
                        messagesNotDelete.push(messages[i]);
                    }
                }
                res.send({
                    data: encodeData.encryptData(JSON.stringify(messagesNotDelete))
                });
            }
            if (err) {
                console.log('Lỗi getMessages');
            }
        })
    }

    //Get users from messages
    getUsersMessage(req, res) {
        let userId = encodeData.decryptData(req.body.userId);
        let usersFromMessage = [];
        let usersInfo = [];
        Message.find({
            $or: [
                { fromUserId: userId }, { toUserId: userId }
            ]
        }, async (err, users) => {
            for (let i = 0; i < users.length; i++) {
                if (userId != users[i].fromUserId) {
                    usersFromMessage.push('' + users[i].fromUserId);
                } else {
                    usersFromMessage.push('' + users[i].toUserId);
                }
            }
            usersFromMessage = [...new Set(usersFromMessage)]
            if (usersFromMessage) {
                for (let i = 0; i < usersFromMessage.length; i++) {
                    let user = await findUser.findIdUser(usersFromMessage[i]);
                    usersInfo.push(user);
                }
                res.send({
                    data: encodeData.encryptData(JSON.stringify(usersInfo))
                })
            }
            if (err) {
                console.log('Lỗi getUsersMessage');
            }
        })
    }

    //Update isView user
    updateIsViewMessage(req, res) {
        let data = req.body;
        let fromUserId = encodeData.decryptData(data.fromUserId);
        let toUserId = encodeData.decryptData(data.toUserId);
        Message.updateMany({ fromUserId: fromUserId, toUserId: toUserId }, { isView: true }).then(data => {
            res.send();
        }).catch(err => console.log(err));
    }

    //Get the last message is watched
    getLastMessage(data) {
        return new Promise((resolve, reject) => {
            Message.find({ fromUserId: data.fromUserId, toUserId: data.toUserId, isView: true }).sort({ _id: -1 }).limit(1).then(message => {
                if (message.length > 0) {
                    resolve(encodeData.encryptData(message[0].time));
                }
            }, err => console.log('Lỗi getLastMessage'))
        })
    }

    //Delete messages
    deleteMessages(req, res) {
        let data = req.body;
        data.fromUserId = encodeData.decryptData(data.fromUserId);
        data.toUserId = encodeData.decryptData(data.toUserId);
        Message.updateMany({
            fromUserId: data.fromUserId,
            toUserId: data.toUserId
        }, { deleted: data.fromUserId }).then(success => {
            res.send({
                success: notification.success_deleted
            })
        }).catch(err => console.log('Lỗi deleteMessages'))
    }
}

module.exports = new MessageController();

